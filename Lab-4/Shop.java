import java.util.Scanner;

public class Shop {
	public static void main(String[] args) {
		Soup[] soupMenu = new Soup[4];
		Scanner scan = new Scanner(System.in);
		for (int i = 0; i < soupMenu.length; i++) {
			System.out.println("For soup "+(i+1)+", what are the ratings?");
			System.out.println("What's the taste?");
			String taste = scan.next();
			System.out.println("How dense (out of 10) is it? 10 being twice as dense as water and 0 being as dense as water.");
			int density = scan.nextInt();
			System.out.println("What's the texture?");
			String texture = scan.next();
			System.out.println("How many stars?");
			int stars = scan.nextInt();
			System.out.println("How hot?");
			int heat = scan.nextInt();
			soupMenu[i] = new Soup(taste, density, texture, stars, heat);
		}
		
		System.out.println("Taste is : " + soupMenu[3].getTaste());
		System.out.println("Density is : " + soupMenu[3].getDensity());
		System.out.println("texture is : " + soupMenu[3].getTexture());
		System.out.println("Stars is : " + soupMenu[3].getStars());
		System.out.println("Heat is : " + soupMenu[3].getHeat());
		soupMenu[3].isItHot();
		System.out.println("What's soup 4's taste?");
		soupMenu[3].setTaste(scan.next());
		System.out.println("What's soup 4's density?");
		soupMenu[3].setDensity(scan.nextInt());
		System.out.println("What's soup 4's texture?");
		soupMenu[3].setTexture(scan.next());
		System.out.println("What's soup 4's stars?");
		soupMenu[3].setStars(scan.nextInt());
		System.out.println("Taste is : " + soupMenu[3].getTaste());
		System.out.println("Density is : " + soupMenu[3].getDensity());
		System.out.println("texture is : " + soupMenu[3].getTexture());
		System.out.println("Stars is : " + soupMenu[3].getStars());
		System.out.println("Heat is : " + soupMenu[3].getHeat());
	}
}
