public class Soup {
	//this is about umami/acidity/sweetness. A metric like thos words will be used
	private String taste;
	//density is out of 10. 10 is twice as dense as water, 0 is as dense as water.
	private int density;
	//texture is about if it's cream soup, actual soup, potage, etc...
	private String texture;
	//how much this soup would be rated
	private int stars;
	//this is in degrees celsius
	private int heat;
	
	public Soup(String ta, int d, String te, int s, int h) {
		this.taste = ta;
		this.density = d;
		this.texture = te;
		this.stars = s;
		this.heat = h;
	}
	
	public String getTaste() {
		return this.taste;
	}
	public int getDensity() {
		return this.density;
	}
	public String getTexture() {
		return this.texture;
	}
	public int getStars() {
		return this.stars;
	}
	public int getHeat() {
		return this.heat;
	}
	
	public void setTaste(String ta) {
		this.taste = ta;
	}
	public void setDensity(int d) {
		this.density = d;
	}
	public void setTexture(String te) {
		this.texture = te;
	}
	public void setStars(int s) {
		this.stars = s;
	}
	
	public void isItHot() {
		if (heat > 50) {
			System.out.println("Yup, it's hot");
		} else if (heat < 26){
			System.out.println("It's cold.");
		} else {
			System.out.println("It's fine.");			
		}
	}
	
	public void isGoodSoup() {
		if (stars >= 3) {
			System.out.println("It should be pretty good");
		} else {
			System.out.println("It's not very popular...");
		}
		System.out.println("It's a "+texture+" kind of soup. It's also "+taste+" in taste.");
		if (density > 4) {
			System.out.println("This soup is pretty dense. It'a good soup.");
		} else {
			System.out.println("It's kind of watery...");			
		}
	}
}